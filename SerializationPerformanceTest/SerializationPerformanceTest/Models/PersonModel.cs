﻿using ProtoBuf;
using RandomPersonGenerator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace SerializationPerformanceTest.Models
{
    [Serializable, DataContract, ProtoContract]
    public class PersonModel
    {
        public PersonModel()
        {
        }

        [DataMember, ProtoMember(1)]
        public string FirstName { get; set; }

        [DataMember, ProtoMember(2)]
        public string LastName { get; set; }

        [DataMember, ProtoMember(3)]
        public Gender Gender { get; set; }

        [DataMember, ProtoMember(4)]
        public int Age { get; set; }

        [DataMember, ProtoMember(5)]
        public DateTime Birthday { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SerializationPerformanceTest
{
    public class PerformanceTestResult
    {
        public string TestName { get; set; }
        public TimeSpan SerializationTimeSpan { get; set; }
        public TimeSpan DeserializationTimeSpan { get; set; }
        public long SizeOfSerializedObject { get; set; }

        public Exception Exception { get; set; }
        public bool IsSuccess
        {
            get
            {
                return (Exception == null) ? true : false;
            }
        }

        public override string ToString()
        {
            if(IsSuccess)
            {
                var format = "{0}\nSerialization: {1} ms\nDeserialization: {2} ms\nSizeOfSerializedData: {3} bytes";
                return string.Format(format, TestName, SerializationTimeSpan.TotalMilliseconds, DeserializationTimeSpan.TotalMilliseconds, SizeOfSerializedObject.ToString("#,0"));
            }
            else
            {
                var format = "{0}\nError!!!\nError message: {1}";
                return string.Format(format, TestName, Exception.Message);
            }
        }
    }
}

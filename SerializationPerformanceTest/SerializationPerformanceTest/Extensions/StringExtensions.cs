﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SerializationPerformanceTest.Extensions
{
    public static class StringExtensions
    {
        public static MemoryStream ConvertToMemoryStream(this string inputString)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter sw = new StreamWriter(stream);
            sw.Write(inputString);
            sw.Flush();
            stream.Position = 0;
            stream.Seek(0, SeekOrigin.Begin);
            return stream;
        }
    }
}

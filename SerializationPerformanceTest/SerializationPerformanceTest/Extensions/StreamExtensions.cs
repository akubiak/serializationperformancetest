﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SerializationPerformanceTest.Extensions
{
    public static class StreamExtensions
    {
        public static string ConvertToString(this Stream stream)
        {
            stream.Seek(0, SeekOrigin.Begin);
            StreamReader sr = new StreamReader(stream);
            var result = sr.ReadToEnd();
            stream.Seek(0, SeekOrigin.Begin);
            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;

namespace SerializationPerformanceTest.Helpers
{
    //Crap code :X 
    //sorry for that - but it was copy & paste
    public class ExcelHelper
    {
        private string FILE_DIRECTORY = Environment.CurrentDirectory + "\\Results\\";
        private string FILE_NAME = "excelResults.xls";
        private Excel.Application _excelApp;

        private ExcelHelper()
        {
            _excelApp = new Excel.Application();
        }

        #region Singleton
        static private ExcelHelper _instance = null;
        public static ExcelHelper Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ExcelHelper();
                }
                return _instance;
            }
        }
        #endregion

        /// <summary>
        /// Saved results to excel file 
        /// Excel needed
        /// </summary>
        public void ExportPerformanceResultsToExcelFile(List<PerformanceTestResult> results)
        {
            if (_excelApp == null)
            {
                Console.WriteLine("Excel is not properly installed!!");
                return;
            }

            Excel.Workbook xlWorkBook = _excelApp.Workbooks.Add();
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            var firstRow = xlWorkSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row;
            xlWorkSheet.Cells[firstRow, 2] = "Serialization Time (ms)";
            xlWorkSheet.Cells[firstRow, 3] = "Deserialization Time (ms)";
            xlWorkSheet.Cells[firstRow, 4] = "Size (bytes)";

            foreach (var result in results)
            {
                var row = xlWorkSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row + 1;
                xlWorkSheet.Cells[row, 1] = result.TestName;
                if (result.IsSuccess)
                {
                    xlWorkSheet.Cells[row, 2] = result.SerializationTimeSpan.TotalMilliseconds;
                    xlWorkSheet.Cells[row, 3] = result.DeserializationTimeSpan.TotalMilliseconds;
                    xlWorkSheet.Cells[row, 4] = result.SizeOfSerializedObject;
                }
            }

            try
            {
                Directory.CreateDirectory(FILE_DIRECTORY);
                xlWorkBook.SaveAs(Filename: FILE_DIRECTORY + FILE_NAME, ConflictResolution: Excel.XlSaveConflictResolution.xlLocalSessionChanges, FileFormat: Excel.XlFileFormat.xlWorkbookNormal, AccessMode: Excel.XlSaveAsAccessMode.xlShared);
            }
            catch(Exception ex)
            {
                throw;
            }
            finally
            {
                xlWorkBook.Close(true);

                ReleaseObject(xlWorkSheet);
                ReleaseObject(xlWorkBook);
                _excelApp.Quit();
                ReleaseObject(_excelApp);
            }
        }


        private void ReleaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}

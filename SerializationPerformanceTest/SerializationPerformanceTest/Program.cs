﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SerializationPerformanceTest.Testers;
using RandomPersonGenerator;
using Newtonsoft.Json;
using SerializationPerformanceTest.Models;
using System.Xml.Serialization;
using System.IO;
using System.Diagnostics;
using SerializationPerformanceTest.Extensions;
using SerializationPerformanceTest.Helpers;

namespace SerializationPerformanceTest
{
    public class Program
    {
        private const bool SKIP_FIRST_ITERATION = true;
        private const bool SAVE_RESULTS_TO_EXCEL_FILE = true;
        private const bool SAVE_SERIALIZATION_RESULT_TO_FILE = false;
        private const int ITERATIONS_COUNT = 100;

        private static readonly PerformanceTester[] _testers = new PerformanceTester[]
        {
            //XML
            new XmlSerializerPerformanceTester(),   
            new DataContractSerializerPerformanceTester(), 
            new ServiceStackXmlSerializerPerformanceTester(), 
            new SharpSerializerXmlSerializerPerformanceTester(), 
  
            //JSON
            new DataContractJsonSerializerPerformanceTester(),  
            new JsonNewtonsoftJsonConvertPerformanceTester(),  
            new ServiceStackJsonSerializerPerformanceTester(),  
            new FastJsonJSONPerformanceTester(), 

            //BINARY
            new BinaryFormatterPerformanceTester(), 
            new ProtoBufSerializerPerformanceTester(),  
            new SharpSerializerBinarySerializerPerformanceTester(), 
            new MsgPackSerializerPerformanceTester(),  
        };

        private static void Main(string[] args)
        {
            Console.WriteLine("------------- Single object -------------");
            TestSingleObject();
            Console.ReadLine();

            //Console.WriteLine("------------- List of objects -------------");
            //TestListOfObjects();
            //Console.ReadLine();
        }

        private static void TestSingleObject()
        {
            PersonModel person = JsonConvert.DeserializeObject<PersonModel>(PersonGenerator.Instance.GenerateRandomPersonAsJson());
            Test(person);
        }

        private static void TestListOfObjects()
        {
            List<PersonModel> personList = JsonConvert.DeserializeObject<List<PersonModel>>(PersonGenerator.Instance.GenerateRandomPersonListAsJson(1500));
            Test(personList);
        }

        private static void Test<TestObjectType>(TestObjectType testObject)
        {
            List<PerformanceTestResult> results = new List<PerformanceTestResult>();
            foreach (var tester in _testers)
            {
                var testResult = tester.Test(testObject, ITERATIONS_COUNT, SKIP_FIRST_ITERATION, SAVE_SERIALIZATION_RESULT_TO_FILE);
                results.Add(testResult);
                Console.WriteLine(testResult.ToString());
                Console.WriteLine();
                GC.Collect();
            }

            if (SAVE_RESULTS_TO_EXCEL_FILE)
            {
                try
                {
                    ExcelHelper.Instance.ExportPerformanceResultsToExcelFile(results);
                    Console.WriteLine("Results saved to excel file");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}

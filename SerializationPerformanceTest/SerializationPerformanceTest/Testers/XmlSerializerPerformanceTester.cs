﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SerializationPerformanceTest.Testers
{
    /// <summary>
    /// XmlSerializer
    /// Format: XML
    /// </summary>
    /// <see cref="http://msdn.microsoft.com/pl-pl/library/system.xml.serialization.xmlserializer"/>
    public class XmlSerializerPerformanceTester : PerformanceTester
    {
        public override MemoryStream Serialize<TestObjectType>(TestObjectType inputObject)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(TestObjectType));
            var stream = new MemoryStream();
            xmlSerializer.Serialize(stream, inputObject);
            return stream;
        }

        public override TestObjectType Deserialize<TestObjectType>(MemoryStream inputStream)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(TestObjectType));
            inputStream.Seek(0, 0);
            var result = xmlSerializer.Deserialize(inputStream);
            return (TestObjectType)result;
        }
    }
}

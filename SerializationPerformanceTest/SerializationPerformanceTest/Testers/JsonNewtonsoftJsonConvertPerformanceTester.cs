﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using SerializationPerformanceTest.Extensions;

namespace SerializationPerformanceTest.Testers
{
    /// <summary>
    /// JsonConvert - Newtonsoft.Json
    /// Format: JSON
    /// </summary>
    /// <see cref="http://james.newtonking.com/json/help/index.html"/>
    public class JsonNewtonsoftJsonConvertPerformanceTester : PerformanceTester
    {
        public override MemoryStream Serialize<TestObjectType>(TestObjectType inputObject)
        {
            var result = JsonConvert.SerializeObject(inputObject);
            return result.ConvertToMemoryStream();
        }

        public override TestObjectType Deserialize<TestObjectType>(MemoryStream inputStream)
        {
            var result = JsonConvert.DeserializeObject<TestObjectType>(inputStream.ConvertToString());
            return (TestObjectType)result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using SerializationPerformanceTest.Extensions;

namespace SerializationPerformanceTest.Testers
{
    /// <summary>
    /// BinaryFormatter 
    /// Format: BINARY
    /// </summary>
    /// <see cref="http://msdn.microsoft.com/pl-pl/library/system.runtime.serialization.formatters.binary.binaryformatter"/>
    public class BinaryFormatterPerformanceTester : PerformanceTester
    {
        public override MemoryStream Serialize<TestObjectType>(TestObjectType inputObject)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            var stream = new MemoryStream();
            binaryFormatter.Serialize(stream, inputObject);
            return stream;
        }

        public override TestObjectType Deserialize<TestObjectType>(MemoryStream inputStream)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            inputStream.Seek(0, 0);
            var result = binaryFormatter.Deserialize(inputStream);
            return (TestObjectType)result;
        }
    }
}

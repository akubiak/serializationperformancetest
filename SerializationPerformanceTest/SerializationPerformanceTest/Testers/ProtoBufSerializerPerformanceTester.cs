﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SerializationPerformanceTest.Testers
{
    /// <summary>
    /// Serializer - ProtoBuf 
    /// Format: BINARY
    /// </summary>
    /// <see cref="https://code.google.com/p/protobuf-net/"/>
    public class ProtoBufSerializerPerformanceTester : PerformanceTester
    {
        public override MemoryStream Serialize<TestObjectType>(TestObjectType inputObject)
        {
            var stream = new MemoryStream();
            Serializer.Serialize<TestObjectType>(stream, inputObject);
            return stream;
        }

        public override TestObjectType Deserialize<TestObjectType>(MemoryStream inputStream)
        {
            inputStream.Seek(0, 0);
            var result = Serializer.Deserialize<TestObjectType>(inputStream);
            return (TestObjectType)result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SerializationPerformanceTest.Extensions;
using fastJSON;

namespace SerializationPerformanceTest.Testers
{
    /// <summary>
    /// JSON - fastJson
    /// Format: JSON
    /// </summary>
    /// <see cref="http://www.codeproject.com/Articles/159450/fastJSON"/>
    public class FastJsonJSONPerformanceTester : PerformanceTester
    {
        public override MemoryStream Serialize<TestObjectType>(TestObjectType inputObject)
        {
            var result = JSON.ToJSON(inputObject);
            return result.ConvertToMemoryStream();
        }

        public override TestObjectType Deserialize<TestObjectType>(MemoryStream inputStream)
        {
            var result = JSON.ToObject<TestObjectType>(inputStream.ConvertToString());
            return default(TestObjectType);
        }
    }
}

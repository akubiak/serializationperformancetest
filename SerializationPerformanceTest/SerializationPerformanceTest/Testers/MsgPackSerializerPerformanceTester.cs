﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using MsgPack.Serialization;

namespace SerializationPerformanceTest.Testers
{
    /// <summary>
    /// MsgPackSerializer
    /// Format: BINARY
    /// </summary>
    /// <see cref="http://cli.msgpack.org/"/>
    public class MsgPackSerializerPerformanceTester : PerformanceTester
    {
        public override MemoryStream Serialize<TestObjectType>(TestObjectType inputObject)
        {
            MessagePackSerializer<TestObjectType> messagePackSerializer = MessagePackSerializer.Get<TestObjectType>();
            var stream = new MemoryStream();
            messagePackSerializer.Pack(stream, inputObject);
            return stream;
        }

        public override TestObjectType Deserialize<TestObjectType>(MemoryStream inputStream)
        {
            MessagePackSerializer<TestObjectType> messagePackSerializer = MessagePackSerializer.Get<TestObjectType>();
            inputStream.Seek(0, 0);
            var result = messagePackSerializer.Unpack(inputStream);
            return (TestObjectType)result;
        }
    }
}

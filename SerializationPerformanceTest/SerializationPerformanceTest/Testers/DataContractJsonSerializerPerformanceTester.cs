﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;

namespace SerializationPerformanceTest.Testers
{
    /// <summary>
    /// DataContractJsonSerializer 
    /// Format: JSON
    /// </summary>
    /// <see cref="http://msdn.microsoft.com/pl-pl/library/system.runtime.serialization.json.datacontractjsonserializer"/>
    public class DataContractJsonSerializerPerformanceTester : PerformanceTester
    {
        public override MemoryStream Serialize<TestObjectType>(TestObjectType inputObject)
        {
            DataContractJsonSerializer dataContractSerializer = new DataContractJsonSerializer(typeof(TestObjectType));
            var stream = new MemoryStream();
            dataContractSerializer.WriteObject(stream, inputObject);
            return stream;
        }

        public override TestObjectType Deserialize<TestObjectType>(MemoryStream inputStream)
        {
            DataContractJsonSerializer dataContractSerializer = new DataContractJsonSerializer(typeof(TestObjectType));
            inputStream.Seek(0, 0);
            var result = dataContractSerializer.ReadObject(inputStream);
            return (TestObjectType)result;
        }
    }
}

﻿using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SerializationPerformanceTest.Testers
{
    /// <summary>
    /// JsonSerializer - ServiceStack.Text
    /// Format: JSON
    /// </summary>
    /// <see cref="http://docs.servicestack.net/text-serializers/json-serializer"/>
    public class ServiceStackJsonSerializerPerformanceTester : PerformanceTester
    {
        public override MemoryStream Serialize<TestObjectType>(TestObjectType inputObject)
        {
            var stream = new MemoryStream();
            JsonSerializer.SerializeToStream(inputObject, stream);
            return stream;
        }

        public override TestObjectType Deserialize<TestObjectType>(MemoryStream inputStream)
        {
            inputStream.Seek(0, 0);
            var result = JsonSerializer.DeserializeFromStream<TestObjectType>(inputStream);
            return (TestObjectType)result;
        }
    }
}

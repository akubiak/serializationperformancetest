﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace SerializationPerformanceTest.Testers
{
    /// <summary>
    /// DataContractSerializer 
    /// Format: XML
    /// </summary>
    /// <see cref="http://msdn.microsoft.com/pl-pl/library/system.runtime.serialization.datacontractserializer"/>
    public class DataContractSerializerPerformanceTester : PerformanceTester
    {
        public override MemoryStream Serialize<TestObjectType>(TestObjectType inputObject)
        {
            DataContractSerializer dataContractSerializer = new DataContractSerializer(typeof(TestObjectType));
            var stream = new MemoryStream();
            dataContractSerializer.WriteObject(stream, inputObject);
            return stream;
        }

        public override TestObjectType Deserialize<TestObjectType>(MemoryStream inputStream)
        {
            DataContractSerializer dataContractSerializer = new DataContractSerializer(typeof(TestObjectType));
            inputStream.Seek(0, 0);
            var result = dataContractSerializer.ReadObject(inputStream);
            return (TestObjectType)result;
        }
    }
}

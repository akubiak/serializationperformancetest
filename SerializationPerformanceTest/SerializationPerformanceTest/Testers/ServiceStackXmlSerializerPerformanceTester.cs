﻿using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SerializationPerformanceTest.Testers
{
    /// <summary>
    /// XmlSerializer - ServiceStack.Text
    /// Format: XML
    /// </summary>
    /// <see cref="https://github.com/ServiceStack/ServiceStack.Text"/>
    public class ServiceStackXmlSerializerPerformanceTester : PerformanceTester
    {
        public override MemoryStream Serialize<TestObjectType>(TestObjectType inputObject)
        {
            var stream = new MemoryStream();
            XmlSerializer.SerializeToStream(inputObject, stream);
            return stream;
        }

        public override TestObjectType Deserialize<TestObjectType>(MemoryStream inputStream)
        {
            inputStream.Seek(0, 0);
            var result = XmlSerializer.DeserializeFromStream<TestObjectType>(inputStream);
            return (TestObjectType)result;
        }
    }
}

﻿using Polenter.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SerializationPerformanceTest.Testers
{
    /// <summary>
    /// SharpSerializer
    /// Format: XML
    /// </summary>
    /// <see cref="http://www.sharpserializer.com/en/index.html"/>
    public class SharpSerializerXmlSerializerPerformanceTester : PerformanceTester
    {
        public override MemoryStream Serialize<TestObjectType>(TestObjectType inputObject)
        {
            SharpSerializer sharpSerializer = new SharpSerializer();
            var stream = new MemoryStream();
            sharpSerializer.Serialize(inputObject, stream);
            return stream;
        }

        public override TestObjectType Deserialize<TestObjectType>(MemoryStream inputStream)
        {
            SharpSerializer sharpSerializer = new SharpSerializer();
            inputStream.Seek(0, 0);
            var result = sharpSerializer.Deserialize(inputStream);
            return (TestObjectType)result;
        }
    }
}

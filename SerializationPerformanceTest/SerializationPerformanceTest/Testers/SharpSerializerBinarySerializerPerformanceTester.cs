﻿using Polenter.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using SerializationPerformanceTest.Extensions;

namespace SerializationPerformanceTest.Testers
{
    /// <summary>
    /// SharpSerializer
    /// Format: BINARY
    /// </summary>
    /// <see cref="http://www.sharpserializer.com/en/index.html"/>
    public class SharpSerializerBinarySerializerPerformanceTester : PerformanceTester
    {
        public override MemoryStream Serialize<TestObjectType>(TestObjectType inputObject)
        {
            SharpSerializer sharpSerializer = new SharpSerializer(true);
            var stream = new MemoryStream();
            sharpSerializer.Serialize(inputObject, stream);
            var text = stream.ConvertToString();
            return stream;
        }

        public override TestObjectType Deserialize<TestObjectType>(MemoryStream inputStream)
        {
            SharpSerializer sharpSerializer = new SharpSerializer(true);
            inputStream.Seek(0, 0);
            var result = sharpSerializer.Deserialize(inputStream);
            return (TestObjectType)result;
        }
    }
}

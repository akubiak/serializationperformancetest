﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SerializationPerformanceTest
{
    public abstract class PerformanceTester
    {
        /// <summary>
        /// Serialize the object to a MemoryStream
        /// </summary>
        public abstract MemoryStream Serialize<TestObjectType>(TestObjectType inputObject);

        /// <summary>
        /// Deserialize the TestObject to a .NET Object 
        /// </summary>
        public abstract TestObjectType Deserialize<TestObjectType>(MemoryStream inputStream);

        /// <summary>
        /// Test for Size/Speed of Serialization/Deserialization
        /// </summary>
        /// <typeparam name="TestObjectType">Type of object to serialize, and deserialize</typeparam>
        /// <param name="testObject">test object</param>
        /// <param name="iterations">iterations should be greater or equal 1, in other cases, value will be set to 1</param>
        /// <param name="saveSerializationOutputToFile">if true, result of serialization will be save in Results directory</param>
        /// <param name="skipFirstIteration">if true, first iteration will be omitted in the results</param>
        /// <returns>PerformanceTestResult</returns>
        public PerformanceTestResult Test<TestObjectType>(TestObjectType testObject, int iterations = 1, bool skipFirstIteration = true, bool saveSerializationOutputToFile = false)
        {
            if(iterations<1)
            {
                iterations = 1;
            }

            if (skipFirstIteration)
            {
               Test<TestObjectType>(testObject, 1);
            }

            PerformanceTestResult result = Test<TestObjectType>(testObject, iterations);
            
            if (saveSerializationOutputToFile)
            {
                var serializeResult = this.Serialize<TestObjectType>(testObject);
                SaveSerializationResultToFile(serializeResult);
            }
            return result;
        }

        private PerformanceTestResult Test<TestObjectType>(TestObjectType testObject, int iterations)
        {
            PerformanceTestResult result = new PerformanceTestResult() { TestName = this.GetType().Name };
            long sizeOfSerializedObject = 0;
            Stopwatch serializationTimer = new Stopwatch();
            Stopwatch deserializationTimer = new Stopwatch();

            try
            {
                for (int i = 0; i < iterations; i++)
                {
                    serializationTimer.Start();
                    var serializeResult = this.Serialize<TestObjectType>(testObject);
                    serializationTimer.Stop();

                    sizeOfSerializedObject += serializeResult.Length;

                    deserializationTimer.Start();
                    var deserializeResult = this.Deserialize<TestObjectType>(serializeResult);
                    deserializationTimer.Stop();
                }

                result.SerializationTimeSpan = new TimeSpan(serializationTimer.Elapsed.Ticks / iterations);
                result.DeserializationTimeSpan = new TimeSpan(deserializationTimer.Elapsed.Ticks / iterations);
                result.SizeOfSerializedObject = sizeOfSerializedObject / iterations;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        private void SaveSerializationResultToFile(MemoryStream stream)
        {
            try
            {
                var directoryPath = "Results\\" + DateTime.Now.ToShortDateString() + "\\";
                Directory.CreateDirectory(directoryPath);
                using (var fileStream = File.Create(directoryPath + this.GetType().Name))
                {
                    stream.Seek(0, 0);
                    stream.CopyTo(fileStream);
                    fileStream.Close();
                    stream.Seek(0, 0);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Error occured, while saving to file.");
            }
        }
    }
}
